//
//  main.swift
//  TaskHospital
//
//  Created by Private on 1/14/18.
//  Copyright © 2018 Private. All rights reserved.
//

import Foundation

protocol Patient {
    var fewer: Bool {get set}
    var headache: Bool {get set}
    var stomachPain: Bool {get set}
    var flue: Bool {get set}
    func gotWorst()
}

protocol DoctorDelegate {
    func healFewer()
    func healHeadache()
    func healStomachPain()
    func healFlue()
    func heal(fewer: Bool, headache: Bool, stomachPain: Bool, flue: Bool)
}

class Doctor: DoctorDelegate {
    var name: String
    
    required init (name: String) {
        self.name = name
    }
    
    func healFewer() {
        print("\(name) - Temperature is normal now")
    }
    func healHeadache () {
        print("\(name) - Headache passed")
    }
    func healStomachPain() {
        print("\(name) - Stomach is ok now")
    }
    func healFlue() {
        print("\(name) - Flue has passed")
    }
    func heal (fewer: Bool, headache: Bool, stomachPain: Bool, flue: Bool) {
        if fewer == true {
            healFewer()
        }
        if headache == true {
            healHeadache()
        }
        if stomachPain == true {
            healStomachPain()
        }
        if flue == true {
            healFlue()
        }
    }
}

class Imposter: DoctorDelegate {
    var name: String
    
    required init (name: String) {
        self.name = name
    }
    
    func healFewer() {
        print("\(name) - Come on, temperature '39.2 is OK, get out")
    }
    func healHeadache () {
        print("\(name) - Go get some cofee, headache will pass")
    }
    func healStomachPain() {
        print("\(name) - Drink some water, stomach should be ok afterward")
    }
    func healFlue() {
        print("\(name) - Go drink some Wisky, flue will pass in a second")
    }
    func heal (fewer: Bool, headache: Bool, stomachPain: Bool, flue: Bool) {
        if fewer == true {
            healFewer()
        }
        if headache == true {
            healHeadache()
        }
        if stomachPain == true {
            healStomachPain()
        }
        if flue == true {
            healFlue()
        }
    }
}

class Sick: Patient {
    
    var name: String
    var fewer: Bool
    var headache: Bool
    var stomachPain: Bool
    var flue: Bool
    
    var delegate: DoctorDelegate?
    
    required init(name: String, delegate: DoctorDelegate,fewer: Bool, headache: Bool, stomachPain: Bool, flue: Bool) {
        self.name = name
        self.fewer = fewer
        self.headache = headache
        self.stomachPain = stomachPain
        self.flue = flue
        self.delegate = delegate
        }
    
    func gotWorst() {
        print("\n\(name) - I am dying, need a doctor!")
        if let delegate = delegate {
            delegate.heal(fewer: fewer, headache: headache, stomachPain: stomachPain, flue: flue)
        }
    }
}

let HouseMD = Doctor(name: "HouseMD")
let MedStudent = Imposter (name: "MedStudent")
let MedStudent2 = Imposter (name: "MedStudent2")
let MedStudent3 = Imposter (name: "MedStudent3")

let Vasia = Sick(name:"Vasia", delegate: MedStudent, fewer: true, headache: true, stomachPain: true, flue: true)
let Petia = Sick(name:"Petia", delegate: MedStudent2, fewer: false, headache: false, stomachPain: true, flue: true)
let Lena = Sick(name:"Lena", delegate: MedStudent3, fewer: true, headache: false, stomachPain: false, flue: true)
let Katia = Sick(name:"Katia", delegate: HouseMD, fewer: true, headache: false, stomachPain: false, flue: false)
let Victor = Sick(name:"Victor", delegate: HouseMD, fewer: false, headache: false, stomachPain: false, flue: true)

var Hospital: [Sick] = [Vasia,Petia,Lena,Katia,Victor]

for i in Hospital {
    i.gotWorst()
}





